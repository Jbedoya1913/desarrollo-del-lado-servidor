var mongoose = require("mongoose");
var Shema = mongoose.Schema;

var bicicletaShema = new Shema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: "2dsphere", sparse: true },
  },
});

bicicletaShema.statics.createInstance = function (
  code,
  color,
  modelo,
  ubicacion
) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion,
  });
};

bicicletaShema.methods.String = function () {
  return "id: " + this.code + " | color: " + this.color;
};

bicicletaShema.statics.allBicis = function (cb) {
  return this.find({}, cb);
};

bicicletaShema.statics.add = function (aBici, cb) {
  return this.create(aBici, cb);
};

bicicletaShema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
};

bicicletaShema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model("Bicicleta", bicicletaShema);
